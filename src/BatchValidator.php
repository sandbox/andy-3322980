<?php declare(strict_types=1);

namespace Drupal\batch_entity_validate;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Provides validation of entities using the batch API.
 */
class BatchValidator implements ContainerInjectionInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The ID of the default user to run the validation as.
   *
   * @var int
   */
  public const DEFAULT_USER_ID = 1;

  /**
   * The account switcher service.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected AccountSwitcherInterface $accountSwitcher;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The route provider service.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected RouteProviderInterface $routeProvider;

  /**
   * Creates the batch validator.
   *
   * @param \Drupal\Core\Session\AccountSwitcherInterface $account_switcher
   *   The account switcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation service.
   */
  final public function __construct(AccountSwitcherInterface $account_switcher, EntityTypeManagerInterface $entity_type_manager, RouteProviderInterface $route_provider, TranslationInterface $string_translation) {
    $this->accountSwitcher = $account_switcher;
    $this->entityTypeManager = $entity_type_manager;
    $this->routeProvider = $route_provider;
    $this->setStringTranslation($string_translation);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('account_switcher'),
      $container->get('entity_type.manager'),
      $container->get('router.route_provider'),
      $container->get('string_translation')
    );
  }

  /**
   * Gets a batch builder for validating entities.
   *
   * @param array $entity_types
   *   An indexed array of entity type IDs to validate.
   * @param int $batch_size
   *   The number of entities to load in one batch.
   *
   * @return \Drupal\Core\Batch\BatchBuilder
   *   A batch builder configured with the appropriate operations.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   If the entity type doesn't exist.
   * @throws \RuntimeException
   *   If the entity type doesn't support validation.
   */
  public function getBatchBuilder(array $entity_types, int $batch_size): BatchBuilder {
    $batch_builder = (new BatchBuilder())
      ->setTitle(dt('Validating'))
      ->setFinishCallback([$this, 'validateFinished']);

    foreach ($entity_types as $entity_type_id) {
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      if (!$this->supportsValidation($entity_type)) {
        throw new \RuntimeException("The \"$entity_type_id\" entity type does not support validation.");
      }
      $batch_builder->addOperation(
        [$this, 'validateEntityType'],
        [$entity_type_id, static::DEFAULT_USER_ID, $batch_size],
      );
    }
    return $batch_builder;
  }

  /**
   * Validates all entities of an entity type.
   *
   * Callback for batch_set().
   *
   * @param string $entity_type_id
   *   The entity type ID to load.
   * @param int $user_id
   *   The ID of the user to run the validation as.
   * @param int $batch_size
   *   The number of entities to load in one go.
   * @param \DrushBatchContext $context
   *   The Drush batch context.
   */
  public function validateEntityType(string $entity_type_id, int $user_id, int $batch_size, \DrushBatchContext $context): void {
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    if (empty($context['sandbox'])) {
      $context['sandbox']['processed'] = 0;
      $context['sandbox']['total'] = $storage->getQuery()
        ->accessCheck(FALSE)
        ->count()
        ->execute();
    }

    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    $id_key = $entity_type->getKey('id');
    $query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->sort($id_key)
      ->range(0, $batch_size);
    if (isset($context['sandbox']['latest_id'])) {
      $query->condition($id_key, $context['sandbox']['latest_id'], '>');
    }
    $ids = $query->execute();

    // In case an entity is deleted or added during validation, don't use
    // processed and total counts to signal the end of validation, just wait
    // until there are no more entities returned.
    if (!$ids) {
      $context['finished'] = 1;
      return;
    }
    $context['finished'] = 0;

    $entities = $storage->loadMultiple($ids);
    $this->accountSwitcher->switchTo(User::load($user_id));
    foreach ($entities as $entity) {
      assert($entity instanceof FieldableEntityInterface);
      $this->validateEntity($entity, $context);
      $context['sandbox']['latest_id'] = $entity->id();
      $context['sandbox']['processed']++;
    }
    $this->accountSwitcher->switchBack();

    $translation_context = [
      '@count' => $context['sandbox']['processed'],
      '@count_label' => $entity_type->getCountLabel($context['sandbox']['total']),
    ];
    $context['message'] = $this->t('Validated @count of @count_label.', $translation_context);
  }

  /**
   * Validates a batch of entities of the same entity type.
   *
   * It can be used when you only want to validate specific entities.
   *
   * Callback for batch_set().
   *
   * @param string $entity_type_id
   *   The entity type ID of the entities.
   * @param int[]|string[] $ids
   *   The IDs of the entities to validate.
   * @param int $user_id
   *   The ID of the user to run the validation as.
   * @param \DrushBatchContext $batch_context
   *   The Drush batch context.
   */
  public function validateEntities(string $entity_type_id, array $ids, int $user_id, \DrushBatchContext $batch_context): void {
    $entities = $this->entityTypeManager->getStorage($entity_type_id)
      ->loadMultiple($ids);
    $this->accountSwitcher->switchTo(User::load($user_id));
    foreach ($entities as $entity) {
      assert($entity instanceof FieldableEntityInterface);
      $this->validateEntity($entity, $batch_context);
    }
    $this->accountSwitcher->switchBack();
  }

  /**
   * Validates an entity and stores the results in the batch context.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity to validate.
   * @param \DrushBatchContext $batch_context
   *   The Drush batch context.
   */
  public function validateEntity(FieldableEntityInterface $entity, \DrushBatchContext $batch_context): void {
    // Validation shouldn't throw an exception, but it might.
    try {
      $violations = $entity->validate();
      foreach ($violations as $violation) {
        assert($violation instanceof ConstraintViolationInterface);
        $values = [
          'message' => $violation->getMessage(),
          'value' => $violation->getInvalidValue(),
          'code' => $violation->getCode(),
        ];
        $batch_context['results'][$entity->getEntityTypeId()][$entity->id()][$violation->getPropertyPath()][] = $values;
      }
    }
    catch (\Throwable $t) {
      $batch_context['results'][$entity->getEntityTypeId()][$entity->id()][''][] = [
        'message' => $this->t('Exception during validation: @message', [
          '@message' => $t->getMessage(),
        ]),
        'value' => NULL,
        'code' => $t->getCode(),
      ];
    }
  }

  /**
   * Handles the end of the batch process.
   *
   * Callback for batch_set().
   */
  public function validateFinished(bool $success, array $results, $operations): void {
  }

  /**
   * Checks if an entity type supports validation.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type to check.
   *
   * @return bool
   *   TRUE if the entity type can be validated; FALSE otherwise.
   */
  public function supportsValidation(EntityTypeInterface $entity_type): bool {
    return $entity_type->entityClassImplements(FieldableEntityInterface::class);
  }

  /**
   * Parse the result of the batch validation.
   *
   * @param array $result
   *   The result from the batch API.
   *
   * @return iterable
   *   Each element is an associative array keyed by:
   *   - message: The violation error message
   *   - value: The incorrect value or NULL
   *   - code: The violation error code or NULL
   *   - entity_type_id: The entity type ID
   *   - entity_id: The entity ID
   *   - path: The property path relative to the entity
   */
  public function parseResult(array $result): iterable {
    foreach ($result as $batch_id => $batch_results) {
      if (is_numeric($batch_id)) {
        foreach ($batch_results as $entity_type_id => $entity_type_violations) {
          foreach ($entity_type_violations as $entity_id => $entity_violations) {
            foreach ($entity_violations as $path => $messages) {
              foreach ($messages as $data) {
                yield $data + [
                  'entity_type_id' => $entity_type_id,
                  'entity_id' => $entity_id,
                  'path' => $path,
                ];
              }
            }
          }
        }
      }
    }
  }

  /**
   * Returns a url to an entity.
   *
   * Where possible it returns an edit-form Url, otherwise it will be canonical.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param int|string $entity_id
   *   The entity ID.
   *
   * @return \Drupal\Core\Url|null
   *   The Url or NULL if none was found.
   */
  public function getEntityUrl(string $entity_type_id, int|string $entity_id): ?Url {
    $routes = [
      "entity.$entity_type_id.edit_form",
      "entity.$entity_type_id.canonical",
    ];
    $params = [$entity_type_id => $entity_id];
    $options = ['absolute' => TRUE];
    foreach ($routes as $route) {
      try {
        // Verify the route exists before returning the Url object.
        $this->routeProvider->getRouteByName($route);
        return Url::fromRoute($route, $params, $options);
      }
      catch (RouteNotFoundException $e) {
      }
    }
    return NULL;
  }

}
