<?php declare(strict_types=1);

namespace Drupal\batch_entity_validate\Commands;

use Drupal\batch_entity_validate\BatchValidator;
use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Provides commands for the batch entity validate module.
 */
class BatchEntityValidateCommands extends DrushCommands {

  /**
   * The batch validator helper object.
   *
   * @var \Drupal\batch_entity_validate\BatchValidator
   */
  protected BatchValidator $validator;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Creates the batch entity validate commands object.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ClassResolverInterface $class_resolver, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct();
    $this->validator = $class_resolver->getInstanceFromDefinition(BatchValidator::class);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Validates content entities.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases,
   *   config, etc.
   *
   * @option batch-size
   *   Number of entities to load in one go
   * @option entity-type
   *   Entity type to validate
   * @option all
   *   Validate all entities
   * @usage batch-entity-validate --entity-type=node
   *   Validate all nodes.
   * @usage batch-entity-validate --entity-type=node --entity-type=user
   *   Validate all nodes and user.
   * @usage batch-entity-validate --all
   *   Validate all content entities.
   *
   * @field-labels
   *   entity_type_id: Entity Type
   *   entity_id: Entity ID
   *   path: Path
   *   message: Message
   *   code: Code
   *   link: Link
   * @default-fields entity_type_id,entity_id,path,message
   *
   * @command batch-entity-validate
   * @aliases bev
   *
   * @filter-default-field entity-type
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   The validation results structured in a RowsOfFields object.
   */
  public function validate(array $options = ['entity-type' => [], 'batch-size' => 50, 'format' => 'table', 'all' => FALSE]): ?RowsOfFields {
    try {
      if ($options['all']) {
        if ($options['entity-type']) {
          throw new \RuntimeException(dt('Do not use the "--entity-type" and "--all" options together.'));
        }
        $entity_types = $this->entityTypeManager->getDefinitions();
        $callback = [$this->validator, 'supportsValidation'];
        $entity_type_ids = array_keys(array_filter($entity_types, $callback));
      }
      else {
        $entity_type_ids = $options['entity-type'];
      }
      if (!$entity_type_ids) {
        throw new \RuntimeException(dt('Use either the "--entity-type" or "--all" options to specify which entity types to validate.'));
      }

      $batch_builder = $this->validator->getBatchBuilder($entity_type_ids, (int) $options['batch-size']);
    }
    catch (\Exception $e) {
      $this->logger()->error($e->getMessage());
      return NULL;
    }

    batch_set($batch_builder->toArray());
    $result = drush_backend_batch_process();

    if (!is_array($result)) {
      $this->logger()->error(dt('Batch process did not return a result array. Returned: !type', ['!type' => gettype($result)]));
      return NULL;
    }
    if (!empty($result[0]['#abort'])) {
      // Whenever an error occurs the batch process does not continue, so this
      // array should only contain a single item, but we still output all
      // available data for completeness.
      $this->logger()->error(dt('Update aborted by: !process', [
        '!process' => implode(', ', $result[0]['#abort']),
      ]));
      return NULL;
    }

    $rows = [];
    foreach ($this->validator->parseResult($result) as $violation) {
      $row = $violation;
      unset($row['value']);
      if ($violation['value']) {
        // @todo: Improve serializing value.
        $value = (string) $violation['value'];
        $row['message'] .= " (value: $value)";
      }

      $url = $this->validator->getEntityUrl($violation['entity_type_id'], $violation['entity_id']);
      $row['link'] = $url?->toString();
      $rows[] = $row;
    }

    return new RowsOfFields($rows);
  }

}
